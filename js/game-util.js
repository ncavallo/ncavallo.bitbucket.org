function tryToCall(func) {
    try {
        func.call();
    } catch (err) {
        console.warn(err.message);
    }
}
var Game = {};
Game.maxLevel = 20;
Game.init = function(calback,options) {
    Game.justWon = false;
    Game.level = 1;
    /*
    if (!(localStorage.getItem('level') === null)) {
        Game.level = localStorage.getItem('level');
        if (Game.level > Game.maxLevel)
            Game.level = 1;
    }
    */
    Game.hiScore = 0;
    if (!(localStorage.getItem('hiScore') === null)) {
        Game.hiScore = localStorage.getItem('hiScore');
    }
    Game.boardId = options.boardId;
    try {
        if (options.showAd) {
            var options = { size: 'preroll', duration: 10000, allowSkip: true, 
                            onHide: calback};
            preroll = new Clay.Advertisement( options );
        } else {
            calback.call();
        }
    } catch(e) {
        console.warn(e.message);
        calback.call();
    }
    
}
Game.incScore = function(score) {
    if (Game.score) {
        Game.score += score;
    } else {
        Game.score = score;
    }
    if (Game.score > Game.hiScore) {
        Game.hiScore = Game.score;
        localStorage.setItem('hiScore',Game.hiScore);
        Game.isHiScore = true;
    }
}
Game.postScore = function() {
    tryToCall(function(){
        if (!Game.leaderboard) {
            Game.leaderboard = new Clay.Leaderboard( { id: Game.boardId } );
        }
        Game.leaderboard.post({ score: Game.score }, function( response ) {
            if (response.success) {
                Game.isHiScore = false;
            } else if (response.error) {
                console.warn("Can't submit score");
            }
        });
    });
}
Game.showScores = function() {
    tryToCall(function() {
        if (!Game.leaderboard) {
            Game.leaderboard = new Clay.Leaderboard( { id: Game.boardId } );
        }
        Game.leaderboard.show({best: true});
    });
}
Game.showAchievements = function() {
    tryToCall(function(){Clay.Achievement.showAll();});
}
Game.passLevel = function() {
    tryToCall(function(){
        Clay.Stats.level( { action: 'pass', level: Game.level } );
    });
    localStorage.setItem('level',Game.level);
    Game.justWon = true;
    return Game.level;
}
Game.startLevel = function() {
    Game.score = 0;
    Game.collectiblesCollected = 0;
    tryToCall(function(){
        Clay.Stats.level( { action: 'start', level: Game.level } );
    });
}
Game.failLevel = function() {
    Game.justWon = false;
    tryToCall(function(){
        Clay.Stats.level( { action: 'fail', level: Game.level } );
    });
}
Game.nextLevel = function() {
    return ++Game.level;
}
Game.areAllItemsCollected = function() {
    return Game.collectibles == Game.collectiblesCollected;
}
Game.setCollectibles = function(count) {
    Game.collectibles = count;
}
Game.collectItem = function() {
    Game.collectiblesCollected++;
}
Game.isInTime = function() {
    return true;
}
Game.currentLevel = function() {
    if (Game.level > Game.maxLevel) {
        Game.level = 1;
    }
    return Game.level;
}
Game.hasMoreLevels = function() {
    if (Game.maxLevel >= Game.level + 1) {
        return true;
    }
    return false;
}
Game.reset = function() {
    Game.level = 1;
}
var DialogUtil = {};
DialogUtil.showDialog = function(titleText, buttonDescriptions) {
        console.log('Showing InterLevel Dialog');
        this.dialog = this.add.group();
        this.dialog.fixedToCamera = true;
        
        var background = this.add.graphics(0, 0);  //init rect
        background.beginFill(0x000000, 1);
        background.drawRect(0, 0, this.world.width, this.world.height);
        background.endFill();
        this.dialog.add(background);
        var title = this.add.text(this.world.centerX, 200, titleText, {font: '64px komika_axisregular', fill: '#ffffff' });
        title.anchor.set(0.5,0.5);
        this.dialog.add(title);
        
        this.starY = 400;
        
        if (Game.justWon)
            this.firstTween();
        
        var count = buttonDescriptions.length;
        var buttonDescription1,buttonDescription2,buttonDescription3;
        buttonDescription1 = buttonDescriptions[0];
        buttonDescription2 = buttonDescriptions[1];
        if (count > 2) {
            buttonDescription3 = buttonDescriptions[2];
        }
        
        var buttonYPostion = 700;
        var i, button, maxButtonColumns, callback, key;
        maxButtonColumns = buttonDescriptions.length+1;
        for( i = 0; i < buttonDescriptions.length; i++) {
            callback = buttonDescriptions[i].callback;
            key = buttonDescriptions[i].key;
            button = this.add.button(this.game.width*((i+1)/maxButtonColumns), buttonYPostion, key, callback, this);
            button.anchor.set(0.5,0.5);
            this.dialog.add(button);
        }
    }