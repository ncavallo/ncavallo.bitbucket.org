var ShadowJump = {};
var GRAVITY = 1100;
var JUMP_SPEED = -850;
ShadowJump.Boot = function(game){};
ShadowJump.Boot.prototype = {
    preload: function() {
        // set scale options
        this.input.maxPointers = 1;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.setScreenSize(true);
    },

    create: function() {
        this.state.start('Preloader');
    } 
};
var PlayerHelper = {};
PlayerHelper.jump = function() {
    if (this.player.body.onFloor() || this.player.body.touching.down) {
        this.player.body.velocity.y = JUMP_SPEED;
        this.lastPlatform.kill();
    }
}
ShadowJump.Preloader = function(game) {
    
};
ShadowJump.Preloader.prototype = {
    create: function() {
        //asset to follow on MainMenu background's animation.
        this.load.image('scroller', 'assets/img/player.png');
        this.load.image('player_final', 'assets/img/player_final.png');
        this.load.image('goal', 'assets/img/jelly_red.png');
        this.load.tilemap('level1', 'maps/level0.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level2', 'maps/level1.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level3', 'maps/level1a.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level4', 'maps/level2.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level5', 'maps/level3.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level6', 'maps/level4.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level7', 'maps/level5.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level8', 'maps/level6.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level9', 'maps/level7.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level10', 'maps/level3a.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level11', 'maps/level9.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level12', 'maps/level10.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level13', 'maps/level8.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level14', 'maps/level11.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level15', 'maps/level12.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level16', 'maps/level13.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level17', 'maps/level14.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level18', 'maps/level16.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level19', 'maps/level15.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('level20', 'maps/level17.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('platform', 'assets/img/skycraper_floor_red.png');
        this.load.image('platform_static', 'assets/img/skycraper_floor_grey.png');
        this.load.image('platform_one_way', 'assets/img/skycraper_floor_green.png');
        this.load.image('platform_petit', 'assets/img/skycraper_floor_2.png');
        this.load.image('title', 'assets/img/shadow-jump-title.png');
        this.load.image('tiles', 'assets/img/ground-tileset.png');
        this.load.image('background', 'assets/img/clouds_bg.png');
        this.load.image('background1', 'assets/img/buildings_bg.png');
        this.load.image('background2', 'assets/img/buildings2_bg.png');
        this.load.image('background3', 'assets/img/buildings3_bg.png');
        this.load.image('show-scores-button', 'assets/img/scores.png');
        this.load.image('show-achievements-button', 'assets/img/achievements.png');
        this.load.image('play-button', 'assets/img/play.png');
        this.load.image('next-button', 'assets/img/next.png');
        this.load.image('retry-button', 'assets/img/retry.png');
        this.load.image('menu-button', 'assets/img/menu.png');
        this.load.image('settings-button', 'assets/img/settings_button.png');
        this.load.image('star', 'assets/img/star.png');
        this.load.spritesheet('coin', 'assets/img/coin.png', 47, 43);
        this.load.image('coin', 'assets/img/jellybig_yellow.png');
        this.load.spritesheet('villain-green', 'assets/img/villain_green_standby.png', 213, 142);
        this.load.spritesheet('villain-red', 'assets/img/villain_red_standby.png', 106, 71);
        this.load.spritesheet('arrow', 'assets/img/arrow_64.png', 64, 96);
        
        //Level Selection
        this.load.spritesheet("levels", "assets/img/levels.png", 64, 64);
	    this.load.spritesheet("level_arrows", "assets/img/level_arrows.png", 48, 48);
        
        this.loadingText = this.add.text(this.world.centerX, this.world.centerY, '', { /*font: "64px Arial",*/ fill: '#ffffff' });
        this.loadingText.anchor.set(0.5,0.5);
        
        this.load.onLoadStart.add(this.loadStart, this);
        this.load.onFileComplete.add(this.fileComplete, this);
        this.load.onLoadComplete.add(this.loadComplete, this);
        
        //Audio
        this.load.audio('collect', ['assets/snd/collect.mp3','assets/snd/collect.ogg']);
        this.load.audio('music', ['assets/snd/music.mp3','assets/snd/music.ogg']);
        this.load.audio('level_complete', ['assets/snd/level_complete.mp3','assets/snd/level_complete.ogg']);
        this.load.audio('game_over', ['assets/snd/game_over.mp3','assets/snd/game_over.ogg']);
        
        
        this.load.start();
        
    },
    loadStart: function() {
        this.loadingText.setText('Loading...');
    },
    fileComplete: function(progress, cacheKey, success, totalLoaded, totalFiles) {
        this.loadingText.setText(progress + "%");
    },
    loadComplete: function() {
        var state = this.state;
        var preroll = {};
        var goToMenu = function() {
            if (preroll.done) {
                preroll.done();
            }
            state.start('Menu');
        };
        var gameOptions = {showAd: true, boardId: 4911};
        Game.init(goToMenu, gameOptions);
        //Sounds
        Game.music = this.add.audio('music');
        Game.collectSound = this.add.audio('collect');
        Game.levelCompleteSound = this.add.audio('level_complete');
        Game.gameOverSound = this.add.audio('game_over');
        //Game.mute();
        this.sound.mute = true;
        Game.music.play('',0,1,true);
    }
};

ShadowJump.Game = function(game){
    this.map = {};
    this.background = {};
    this.background1 = {};
    this.background2 = {};
    this.background3 = {};
    this.layer = {};
    this.player = {};
    this.platforms = [];
    this.lastPlatform = {};
    this.goal = {};
    this.coins = [];
    this.enemies = [];
};

ShadowJump.Game.prototype = {
    create : function () {
        
        Game.startLevel();  
        
        console.log(Game.currentLevel());
        
        this.world.setBounds(0, 0, 640, 960);
        this.stage.backgroundColor = '#737CA1';

        this.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.map = this.add.tilemap('level' + Game.currentLevel());

        this.map.addTilesetImage('ground-tileset', 'tiles');

        this.layer = this.map.createLayer('clouds');
        this.layer.scrollFactorY = 0.5;
        this.bg1 = this.map.createLayer('buildings');
        this.bg1.scrollFactorY = 0.25;
        this.bg2 = this.map.createLayer('buildings2');
        this.bg2.scrollFactorY = 0.10;
        this.bg3 = this.map.createLayer('buildings3');
        this.bg3.scrollFactorY = 0.05;
        
        this.layer.resizeWorld();
        this.background3 = this.add.sprite(0, 0, 'background3'); 
        this.background2 = this.add.sprite(0, 0, 'background2'); 
        this.background1 = this.add.sprite(0, 0, 'background1'); 
        this.background = this.add.sprite(0, 0, 'background');

        //Create Moving Platforms
        this.platforms = this.createPlatforms();
        this.coins = this.createCollectibles();
        this.enemies = this.createEnemies();
        this.goal = this.createGoal();
        this.physics.enable(this.goal, Phaser.Physics.ARCADE);
        this.goal.body.allowGravity = false;
        this.goal.body.immovable = true;
        this.goal.body.setSize(this.goal.width,this.goal.height-16,0,8);

        this.markers = this.createMarkers(); 
        
        // Create a player sprite
        this.player = this.createPlayer();

        // Enable physics on the player
        this.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.player.body.setSize(this.player.width/3,this.player.height*3/4,this.player.width/3,this.player.height/4);
        
        this.target = this.add.sprite(this.player.x,this.player.y);
        
        this.camera.follow(this.target);
        
        this.scoreContainer = this.add.group();
        this.scoreContainer.fixedToCamera = true;
        var scoreText = this.add.text(this.world.centerX, 32, '0');
        scoreText.anchor.set(0.5);
        scoreText.font = 'komika_axisregular';
        scoreText.fontSize = 64;
        scoreText.fill = '#ff0044';
        this.scoreContainer.add(scoreText);
        
        this.touchArea = this.add.sprite(0,0);
        this.touchArea.width = this.game.width;
        this.touchArea.height = this.game.height;
        this.touchArea.fixedToCamera = true;
        this.touchArea.inputEnabled  = true;
        this.touchArea.useHandCursor = true;
        this.touchArea.events.onInputDown.add(PlayerHelper.jump, this);
        
        this.resumeGame();
        
    },
    createMarkers: function () {
        var p = this.add.group();
        var ps = this.map.objects['objects'];
        var index;
        for (index = 0; index < ps.length; index++) {
            var elem = ps[index];
            if (elem.name == 'mark') {
                var mark = this.add.sprite(elem.x, elem.y, 'arrow');
                this.physics.enable(mark, Phaser.Physics.ARCADE);
                mark.animations.add('idle',[0,1,2], 6, true);
                mark.play('idle');
                p.add(mark);
            }
        }
        return p;
    },
    createPlayer: function () {
        var o = this.map.objects['objects'];
        for (var i = 0; i < o.length; ++i) {
            if (o[i].name == 'player') {
                var pl = this.add.sprite(o[i].x, o[i].y, 'player_final');
                return pl;
            }
        }
        return null;
    },
    createGoal: function () {
        var o = this.map.objects['objects'];
        for (var i = 0; i < o.length; ++i) {
            if (o[i].name === 'goal') {
                var goal = this.add.sprite(o[i].x, o[i].y, 'goal');
                var goalMaxY = goal.y - 15;
                this.add.tween(goal).to( { y: goalMaxY }, 750, Phaser.Easing.Linear.None, true, -1, 1000, true);
                return goal;
            }
        }
        return null;
    },
    createEnemies: function() {
        var p = this.add.group();
        var ps = this.map.objects['enemies'];
        var index;
        for (index = 0; index < ps.length; index++) {
            var elem = ps[index];
            var e = {};
            if (elem.name !== "enemy2") {
                e = this.add.sprite(elem.x, elem.y, 'villain-green');
            } else {
                e = this.add.sprite(elem.x, elem.y, 'villain-red');
            }
            
            this.physics.enable(e, Phaser.Physics.ARCADE);
            e.body.setSize(e.width,e.height*6/7,0,0);
            e.animations.add('standby-left',[2,3], 4, true);
            e.animations.add('standby-right',[0,1], 4, true);
            if (elem.x > this.world.width/2) {
                e.animations.play('standby-left');
            } else {
                e.animations.play('standby-right');
            }
            p.add(e);
        }
        return p;
    },
    createCollectibles: function () {
        var p = this.add.group();
        var ps = this.map.objects['collectibles'];
        var index;
        Game.setCollectibles(ps.length);
        for (index = 0; index < ps.length; index++) {
            var elem = ps[index];
            var coin = this.add.sprite(elem.x, elem.y, 'coin');
            this.physics.enable(coin, Phaser.Physics.ARCADE);
            coin.body.allowGravity = false;
            coin.body.immovable = true;
            p.add(coin);
        }
        return p;
    },
    createPlatforms: function () {
        var p = this.add.group();
        var ps = this.map.objects['platforms'];
        var index;
        for (index = 0; index < ps.length; index++) {
            var elem = ps[index];
            var platform = this.add.sprite(elem.x, elem.y, elem.name);
            this.physics.enable(platform, Phaser.Physics.ARCADE);
            platform.body.allowGravity = false;
            platform.body.collideWorldBounds = true;
            platform.body.checkCollision.up = true;
            platform.body.checkCollision.right = false;
            platform.body.checkCollision.left = false;
            platform.body.checkCollision.down = false;
            platform.body.immovable = true;
            platform.body.velocity.x = elem.properties['velocityX'];
            platform.velocityX = elem.properties['velocityX'];
            platform.direction = 1;
            platform.switchDirection = function() {
                this.direction = -this.direction;
                this.body.velocity.x = this.velocityX * this.direction;
            };
            if (elem.properties['maxLeft']) {
                platform.maxLeft = elem.properties['maxLeft'];
            }
            if (elem.properties['maxRight']) {
                platform.maxRight = elem.properties['maxRight'];
            }
            
            if (elem.properties['resetToCero']) {
                platform.resetWhenOutOfScreen = true;
                platform.body.collideWorldBounds = false;
            }
            
            platform.onWall = function() {
                var body = this.body;
                return (body.onWall() 
                        || ((this.maxRight && body.x + body.width > this.maxRight) && (body.velocity.x > 0)) 
                        || ((this.maxLeft && body.x < this.maxLeft) && (body.velocity.x < 0)));
            }
            
            p.add(platform);
        }
        return p;
    },
    update: function () {
        
        //Updates target so the camera can follow it.
        if (this.player.y < this.target.y) {
            this.target.y = this.player.y;
        }
        
        this.physics.arcade.collide(this.player, this.platforms, function(p1,p2){ this.lastPlatform = p2}, null, this);
        this.physics.arcade.collide(this.enemies, this.platforms);
        this.physics.arcade.collide(this.markers, this.platforms);
        
        if (!this.paused) {
            
            if (this.player.y > this.world.height) {
                this.pauseGame();
                Game.gameOverSound.play();
                Game.failLevel();
                this.transitionToInterlevel();
            }
            this.physics.arcade.overlap(this.player,this.goal, function(p,g){
                Game.levelCompleteSound.play();
                g.kill();
                this.freezeGame();
                Game.passLevel();
                this.transitionToInterlevel();
            }, null, this);

            this.physics.arcade.overlap(this.player,this.enemies, function(p,e){
                this.pauseGame();
                Game.gameOverSound.play();
                Game.failLevel();
                this.transitionToInterlevel();
            }, null, this);

            this.background.y = this.layer.scrollY;
            this.background.x += 1;
            if (this.background.x > this.world.width) {
                this.background.x = -this.background.width;
            }
            this.background1.y = this.bg1.scrollY;
            this.background2.y = this.bg2.scrollY;
            this.background3.y = this.bg3.scrollY;
            this.physics.arcade.overlap(this.player, this.coins, function(p,c) {
                Game.collectSound.play();
                c.kill();
                Game.collectItem();
                Game.incScore(10);
                this.scoreContainer.setAll('text', Game.score);
            }, null, this);

            var world = this.world;
            
            this.platforms.forEach(function(child) {
                if (child.resetWhenOutOfScreen) {
                    if (child.x > world.width) {
                        child.x = -child.width;
                    } else if (child.x < -child.width) {
                        child.x = world.width;
                    }
                } else {
                    if (child.onWall.call(child)) {
                        child.switchDirection.call(child);
                    }
                }
            });
/*
            if (this.input.activePointer.justPressed()) {
                if (this.player.body.onFloor() || this.player.body.touching.down) {
                    this.player.body.velocity.y = JUMP_SPEED;
                    this.lastPlatform.kill();
                }
            }
*/
        }
    },
    transitionToInterlevel: function() {
        this.freezeGame();
        var background = this.add.graphics(0, 0);  //init rect
        background.beginFill(0x000000, 1);
        background.drawRect(0, 0, this.world.width, this.world.height);
        background.endFill();
        var tween = this.add.tween(background);
        tween.from({alpha: 0}, 750, Phaser.Easing.Cubic.None);
        tween.onComplete.add(function(){this.state.start('InterLevel')}, this);
        tween.start();
        
    },
    freezeGame: function() {
        this.physics.arcade.gravity.y = 0;
        this.player.body.velocity.y = 0;
    },
    pauseGame: function() {
        this.freezeGame();
        this.paused = true;
    },
    resumeGame: function() {
        this.physics.arcade.gravity.y = GRAVITY;
        this.paused = false;
    },
    togglePause: function() {
        if (this.paused) {
            this.resumeGame();
        } else {
            this.pauseGame();
        }
    }
};
ShadowJump.Menu = function(game){};
ShadowJump.Menu.prototype = {
    create: function() {
        
        this.map = this.add.tilemap('level1');

        this.map.addTilesetImage('ground-tileset', 'tiles');

        this.layer = this.map.createLayer('clouds');
        this.layer.scrollFactorY = 0.5;
        this.bg1 = this.map.createLayer('buildings');
        this.bg1.scrollFactorY = 0.25;
        this.bg2 = this.map.createLayer('buildings2');
        this.bg2.scrollFactorY = 0.10;
        this.bg3 = this.map.createLayer('buildings3');
        this.bg3.scrollFactorY = 0.05;
        
        this.layer.resizeWorld();
        this.background3 = this.add.sprite(0, 0, 'background3');
        this.background3.visible = true;
        this.background2 = this.add.sprite(0, 0, 'background2'); 
        this.background2.visible = true;
        this.background1 = this.add.sprite(0, 0, 'background1');
        this.background1.visible = true;
        this.background = this.add.sprite(0, 0, 'background');
        this.background.visible = true;
        
        var title = this.add.sprite(this.world.centerX,300,'title');
        title.anchor.set(0.5,0.5);
        title.fixedToCamera = true;
        var btnGroup = this.add.group();
        btnGroup.fixedToCamera = true;
        
        var buttonYPosition = 700;
        
        var playButton = this.add.button(this.world.centerX,buttonYPosition,'play-button', function() { this.state.start('Game')}, this);
        playButton.anchor.set(0.5,0.5);
        var showScoresButton = this.add.button(this.world.width/4,buttonYPosition,'show-scores-button',function(){Game.showScores();}, this);
        showScoresButton.anchor.set(0.5,0.5);
        var showAchievementsButton = this.add.button(this.world.width*3/4,buttonYPosition,'show-achievements-button',function(){Game.showAchievements();}, this);
        showAchievementsButton.anchor.set(0.5,0.5);
        
        btnGroup.add(playButton);
        btnGroup.add(showScoresButton);
        btnGroup.add(showAchievementsButton);
        
        
        this.scroller = this.add.sprite(0,this.world.height - 480,'scroller');
        this.scroller.visible = false;
        this.camera.follow(this.scroller);
        this.scrollerDirection = -1;
        this.scrollerVelocity = 2;
        
    },
    
    update: function() {
        if (this.scroller.y > this.world.height - 480) {
            this.scrollerDirection = -1;
        } else if (this.scroller.y < 480) {
            this.scrollerDirection = 1;
        }
        this.scroller.y += this.scrollerDirection*this.scrollerVelocity;
        this.background.y = this.layer.scrollY;
        this.background.x += 1;
        if (this.background.x > this.world.width) {
            this.background.x = -this.background.width;
        }
        this.background1.y = this.bg1.scrollY;
        this.background2.y = this.bg2.scrollY;
        this.background3.y = this.bg3.scrollY;
    }
};
//this.showGameOverDialog();
ShadowJump.GameOver = function(game){};
ShadowJump.GameOver.prototype = {
    create: function() {
        this.showGameOverDialog();  
    },
    showGameOverDialog: function() {
        DialogUtil.showDialog.call(this,'Game Over', [{key: 'retry-button', callback: function() {this.state.start('Game');}}, {key: 'next-button', callback: function() {Game.reset();this.state.start('Game');}}, {key: 'menu-button', callback: function() {this.state.start('Menu');}}]);
    }
}
ShadowJump.InterLevel = function(game){};
ShadowJump.InterLevel.prototype = {
    create: function() {
        if (Game.justWon) {
            this.showWinDialog();
            Game.postScore();
        } else {
            this.showLoseDialog();
        }
    },
    showLoseDialog: function() {
        DialogUtil.showDialog.call(this,'You lose', [{key: 'retry-button', callback: function() {this.state.start('Game');}}, {key: 'menu-button', callback: function() {this.state.start('Menu');}}]);
    },
    showWinDialog: function() {
        DialogUtil.showDialog.call(this,'Level ' + Game.currentLevel() + ' complete', [{key: 'retry-button', callback: function() {this.state.start('Game');}}, {key: 'next-button', callback: function() {
            if (Game.hasMoreLevels()) {
                Game.nextLevel();
                this.state.start('Game');
            } else {
                this.state.start('GameOver');
            }
            
        }}, {key: 'menu-button', callback: function() {this.state.start('Menu');}}]);
    },
    firstTween : function() {
        this.star1 = this.add.sprite(this.world.width/4,this.starY,'star');
        this.star1.anchor.set(0.5);
        var t = this.add.tween(this.star1);
        t.from({width: 0, height: 0}, 1000, Phaser.Easing.Bounce.Out);
        if (Game.areAllItemsCollected() || Game.isInTime()) {
            t.onComplete.add(this.secondTween, this);
        }
        t.start();
    },
    secondTween : function() {
        this.star2 = this.add.sprite(this.world.width/2,this.starY,'star');
        this.star2.anchor.set(0.5);
        var t = this.add.tween(this.star2);
        t.from({width: 0, height: 0}, 1000, Phaser.Easing.Bounce.Out);
        if (Game.areAllItemsCollected() && Game.isInTime()) {
            t.onComplete.add(this.thirdTween, this);
        }
        t.start();
    },
    thirdTween : function() {
        this.star3 = this.add.sprite(this.world.width*3/4,this.starY,'star');
        this.star3.anchor.set(0.5);
        var t = this.add.tween(this.star3);
        t.from({width: 0, height: 0}, 1000, Phaser.Easing.Bounce.Out);
        t.start();
    }
};
ShadowJump.LevelSelection = function(game) {

};
ShadowJump.LevelSelection.prototype = {
    create: function() {
        
        this.leftArrow = this.add.button(50,900,"level_arrows",this.arrowClicked);
        this.leftArrow.anchor.setTo(0.5);
        this.leftArrow.frame = 0;
        this.leftArrow.alpha = 0.3;
	
        this.rightArrow = this.add.button(590,900,"level_arrows",this.arrowClicked);
        this.rightArrow.anchor.setTo(0.5);
        this.rightArrow.frame = 1;
        this.rightArrow.alpha = 0.3;
        
        this.levelThumbsGroup = this.add.group();
        this.cols = 4;
        this.rows = 5;
        this.totalLevels = Game.levels;
    },
    arrowClicked: function() {
        console.log('click');
    }
};
/*
ShadowJump.LevelSelect = function(game) {

};
ShadowJump.LevelSelect.prototype = {
// group where to place all level thumbnails
var levelThumbsGroup;
// current page
var currentPage;
// arrows to navigate through level pages
var leftArrow;
var rightArrow;

create: function(){
  		// how many pages are needed to show all levels?
		// CAUTION!! EACH PAGE SHOULD HAVE THE SAME AMOUNT OF LEVELS, THAT IS
		// THE NUMBER OF LEVELS *MUST* BE DIVISIBLE BY THUMBCOLS*THUMBROWS
  		this.pages = game.global.starsArray.length/(game.global.thumbRows*game.global.thumbCols);
        var pages = this.pages;
  		// current page according to last played level, if any
		currentPage = Math.floor(game.global.level/(game.global.thumbRows*game.global.thumbCols));
		if(currentPage>pages-1){
			currentPage = pages-1;
		}
		// left arrow button, to turn one page left
		leftArrow = game.add.button(50,420,"level_arrows",this.arrowClicked,this);
		leftArrow.anchor.setTo(0.5);
		leftArrow.frame = 0;
		// can we turn one page left?
		if(currentPage==0){
			leftArrow.alpha = 0.3;
		}
		// right arrow button, to turn one page right
		rightArrow = game.add.button(270,420,"level_arrows",this.arrowClicked,this);
		rightArrow.anchor.setTo(0.5);
		rightArrow.frame = 1;
		// can we turn one page right?
		if(currentPage==pages-1){
			rightArrow.alpha = 0.3;
		}
		// creation of the thumbails group
		levelThumbsGroup = game.add.group();
		// determining level thumbnails width and height for each page
		var levelLength = game.global.thumbWidth*game.global.thumbCols+game.global.thumbSpacing*(game.global.thumbCols-1);
		var levelHeight = game.global.thumbWidth*game.global.thumbRows+game.global.thumbSpacing*(game.global.thumbRows-1);
		// looping through each page
		for(var l = 0; l < pages; l++){
			// horizontal offset to have level thumbnails horizontally centered in the page
			var offsetX = (game.width-levelLength)/2+game.width*l;
			// I am not interested in having level thumbnails vertically centered in the page, but
			// if you are, simple replace my "20" with
			// (game.height-levelHeight)/2
			var offsetY = 20;
			// looping through each level thumbnails
		     for(var i = 0; i < game.global.thumbRows; i ++){
		     	for(var j = 0; j < game.global.thumbCols; j ++){  
		     		// which level does the thumbnail refer?
					var levelNumber = i*game.global.thumbCols+j+l*(game.global.thumbRows*game.global.thumbCols);
					// adding the thumbnail, as a button which will call thumbClicked function if clicked   		
					var levelThumb = game.add.button(offsetX+j*(game.global.thumbWidth+game.global.thumbSpacing), offsetY+i*(game.global.thumbHeight+game.global.thumbSpacing), "levels", this.thumbClicked, this);	
					// shwoing proper frame
					levelThumb.frame=game.global.starsArray[levelNumber];
					// custom attribute 
					levelThumb.levelNumber = levelNumber+1;
					// adding the level thumb to the group
					levelThumbsGroup.add(levelThumb);
					// if the level is playable, also write level number
					if(game.global.starsArray[levelNumber]<4){
						var style = {
							font: "18px Arial",
							fill: "#ffffff"
						};
						var levelText = game.add.text(levelThumb.x+5,levelThumb.y+5,levelNumber+1,style);
						levelText.setShadow(2, 2, 'rgba(0,0,0,0.5)', 1);
						levelThumbsGroup.add(levelText);
					}
				}
			}
		}
		// scrolling thumbnails group according to level position
		levelThumbsGroup.x = currentPage * game.width * -1
	},
	arrowClicked:function(button){
        var pages = this.pages;
		// touching right arrow and still not reached last page
		if(button.frame==1 && currentPage<pages-1){
			leftArrow.alpha = 1;
			currentPage++;
			// fade out the button if we reached last page
			if(currentPage == pages-1){
				button.alpha = 0.3;
			}
			// scrolling level pages
			var buttonsTween = game.add.tween(levelThumbsGroup);
			buttonsTween.to({
				x: currentPage * game.width * -1
			}, 500, Phaser.Easing.Cubic.None);
			buttonsTween.start();
		}
		// touching left arrow and still not reached first page
		if(button.frame==0 && currentPage>0){
			rightArrow.alpha = 1;
			currentPage--;
			// fade out the button if we reached first page
			if(currentPage == 0){
				button.alpha = 0.3;
			}
			// scrolling level pages
			var buttonsTween = game.add.tween(levelThumbsGroup);
			buttonsTween.to({
				x: currentPage * game.width * -1
			}, 400, Phaser.Easing.Cubic.None);
			buttonsTween.start();
		}		
	},
	thumbClicked:function(button){
		// the level is playable, then play the level!!
		if(button.frame < 4){
			game.global.level = button.levelNumber;
			game.state.start("PlayLevel");
		}
		// else, let's shake the locked levels
		else{
			var buttonTween = game.add.tween(button)
			buttonTween.to({
				alpha: 0.5
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.to({
				alpha: 1
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.to({
				alpha: 0.5
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.to({
				alpha: 1
			}, 20, Phaser.Easing.Cubic.None);
			buttonTween.start();
		}
	}
} 
};
*/